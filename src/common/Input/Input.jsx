import './Input.css';

function Input(props) {
  return (
    <div className='input'>
      <label htmlFor='inp'>{props.labelText}</label>
      <input
        id={props.id}
        type={props.type}
        value={props.value}
        placeholder={props.placeholderText}
        onChange={props.handleChange}
        onBlur={props.onBlur}
      />
    </div>
  );
}

export default Input;
