import './Button.css';

function Button(props) {
  return (
    <button
      className='button'
      type={props.type || 'button'}
      onClick={props.handleClick}
      disabled={props.disabled}
    >
      {props.buttonText}
    </button>
  );
}

export default Button;
