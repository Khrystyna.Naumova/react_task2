import React, { useState } from 'react';
import Button from '../../common/Button/Button';
import styles from './Registration.module.css';
import Input from '../../common/Input/Input';
import { Link, useNavigate } from 'react-router-dom';

const Registration = (props) => {
  const [inputName, setInputName] = useState('');
  const [inputEmail, setInputEmail] = useState('');
  const [inputPassword, setInputPassword] = useState('');
  const [nameIsValid, setNameIsValid] = useState();
  const [emailIsValid, setEmailIsValid] = useState();
  const [passwordIsValid, setPasswordIsValid] = useState();

  const [formIsValid, setFormIsValid] = useState(false);

  const navigate = useNavigate();

  const nameChangeHandler = (event) => {
    setInputName(event.target.value);
    setFormIsValid(
      event.target.value.trim().length > 2 &&
        inputEmail.includes('@') &&
        inputPassword.trim().length > 5
    );
  };

  const emailChangeHandler = (event) => {
    setInputEmail(event.target.value);
    setFormIsValid(
      event.target.value.includes('@') && inputPassword.trim().length > 5
    );
  };

  const passwordChangeHandler = (event) => {
    setInputPassword(event.target.value);
    setFormIsValid(
      event.target.value.trim().length > 5 && inputEmail.includes('@')
    );
  };

  const validateNameHandler = () => {
    setNameIsValid(inputName.trim().length > 2);
  };

  const validateEmailHandler = () => {
    setEmailIsValid(inputEmail.includes('@'));
  };

  const validatePasswordHandler = () => {
    setPasswordIsValid(inputPassword.trim().length > 5);
  };

  const submitHandler = (event) => {
    event.preventDefault();
    const newUser = {
      name: inputName,
      email: inputEmail,
      password: inputPassword,
    };
    registUser(newUser);
  };

  async function registUser(newUser) {
    await fetch('http://localhost:4000/register', {
      method: 'POST',
      body: JSON.stringify(newUser),
      headers: { 'Content-Type': 'application/json' },
    })
      .then((response) => response.json())
      .then((finalRes) => {
        console.log(finalRes);
      });
    navigate('/login');
  }

  return (
    <div className={styles.regist}>
      <h1>Registration</h1>
      <form onSubmit={submitHandler} className={styles.registForm}>
        <div className={`${nameIsValid === false ? styles.invalid : ''}`}>
          <label htmlFor='name'>Name</label>
          <Input
            type='text'
            id='name'
            placeholderText='Enter Name'
            value={inputName}
            handleChange={nameChangeHandler}
            onBlur={validateNameHandler}
          />
        </div>
        <div className={`${emailIsValid === false ? styles.invalid : ''}`}>
          <label htmlFor='email'>Email</label>
          <Input
            type='email'
            id='email'
            placeholderText='Enter Email'
            value={inputEmail}
            handleChange={emailChangeHandler}
            onBlur={validateEmailHandler}
          />
        </div>
        <div className={` ${passwordIsValid === false ? styles.invalid : ''}`}>
          <label htmlFor='password'>Password</label>
          <Input
            type='password'
            id='password'
            placeholderText='Enter password'
            value={inputPassword}
            handleChange={passwordChangeHandler}
            onBlur={validatePasswordHandler}
          />
        </div>
        <div className={styles.actions}>
          <Button
            type='submit'
            disabled={!formIsValid}
            buttonText='Registration'
          />
        </div>
        <p>
          {'If you have an account you can '}
          <Link to='/login'>Login</Link>
        </p>
      </form>
    </div>
  );
};

export default Registration;
