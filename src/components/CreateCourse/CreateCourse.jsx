import React, { useState } from 'react';
import Button from '../../common/Button/Button';
import Input from '../../common/Input/Input';
import './CreateCourse.css';
import pipeDuration from '../../helpers/pipeDuration';
import { mockedAuthorsList } from '../../constants.js';
import Authors from '../Authors/Authors';
import DeleteAuthors from '../Authors/DeleteAuthors';
import { authorArraySort } from '../../helpers/authorConstructor';
import { useNavigate } from 'react-router-dom';

function CreateCourse(props) {
  const [inputTitle, setTitle] = useState('');
  const [inputDescription, setDescription] = useState('');
  const [inputAuthor, setAuthor] = useState('');
  const [inputDuration, setDuration] = useState('');
  const [delAuthors, setDelAuthors] = useState([]);

  const [authorsList, setAuthorsList] = useState(mockedAuthorsList);

  const navigate = useNavigate();

  const titleHandler = (event) => {
    setTitle(event.target.value);
  };
  const descriptionHandler = (event) => {
    setDescription(event.target.value);
  };
  const authorHandler = (event) => {
    setAuthor(event.target.value);
  };
  const durationHandler = (event) => {
    setDuration(event.target.value);
  };

  const submitHandler = (event) => {
    event.preventDefault();
    const courseData = {
      title: inputTitle,
      description: inputDescription,
      creationDate: new Intl.DateTimeFormat(['ban', 'id']).format(new Date()),
      duration: inputDuration,
      authors: authorArraySort(delAuthors),
    };
    if (
      courseData.title.trim().length === 0 ||
      courseData.description.trim().length === 0 ||
      courseData.duration.trim().length === 0 ||
      courseData.authors === 0
    ) {
      alert('Please fill in all fields.');
      return;
    }

    props.onSaveCourse(courseData);
    setTitle('');
    setDescription('');
    setDuration('');
    navigate('/courses');
  };

  const authorSubmitHandler = (event) => {
    event.preventDefault();
    const authorData = {
      name: inputAuthor,
    };
    if (authorData.name.trim().length === 0) {
      alert('Please fill in author name.');
      return;
    }
    onSaveAuthor(authorData);
    setAuthor('');
  };

  const onSaveAuthor = (inputData) => {
    const newAuthor = {
      ...inputData,
      id: Math.random().toString(),
    };
    setAuthorsList((prevAuthor) => {
      return [...prevAuthor, newAuthor];
    });
  };

  const addOneAuthor = (id) => {
    const newAuthorList = authorsList.filter((item) => item.id !== id);
    setAuthorsList(newAuthorList); // Delete from 'Authors'
    const authorDel = authorsList.filter((item) => item.id === id);
    delAuthors.unshift(...authorDel);
    setDelAuthors(delAuthors); // Add to 'Course Authors'
  };

  const deleteOneAuthor = (id) => {
    setDelAuthors((prevState) => {
      const updateDelAuthors = prevState.filter((item) => item.id !== id);
      return updateDelAuthors;
    }); // Delete from 'Course Authors'
    const authorReturn = delAuthors.filter((item) => item.id === id);
    authorsList.push(...authorReturn);
    setAuthorsList(authorsList); // Add to 'Authors'
  };

  // CreateCourse.propTypes = {
  //   inputTitle: PropTypes.string.isRequired,
  // };

  return (
    <div>
      <form className='createCourse' onSubmit={submitHandler}>
        <div className='createCourse__bar'>
          <Input
            placeholderText='Enter title...'
            labelText='Title'
            value={inputTitle}
            handleChange={titleHandler}
          />
          <Button
            // handleClick={submitHandler}
            type='submit'
            buttonText='Create course'
          />
        </div>
        <div className='createCourse__descr'>
          <label>Description</label>
          <textarea
            rows={6}
            placeholder='Enter description'
            value={inputDescription}
            onChange={descriptionHandler}
          />
        </div>
        <div className='createCourse__main'>
          <div className='createCourse__main1'>
            <h3>Add author</h3>
            <Input
              placeholderText='Enter author name...'
              labelText='Author name'
              value={inputAuthor}
              handleChange={authorHandler}
            />
            <br></br>
            <Button
              buttonText='Create author'
              // type='submit'
              handleClick={authorSubmitHandler}
            />
            <br></br>
            <br></br>
            <h3>Duration</h3>
            <Input
              placeholderText='Enter duration in minutes...'
              labelText='Duration'
              value={inputDuration}
              handleChange={durationHandler}
            />
            <br></br>
            <div>
              <p>
                Duration: <span> {pipeDuration(inputDuration)} </span>
              </p>
            </div>
          </div>
          <div className='createCourse__main2'>
            <h3>Authors</h3>
            {authorsList.map((author) => (
              <Authors
                addAuthor={() => addOneAuthor(author.id)} //inline handler
                key={author.id}
                name={author.name}
              />
            ))}

            <h3>Course authors</h3>
            {delAuthors.length > 0 ? (
              delAuthors.map((author) => (
                <DeleteAuthors
                  deleteAuthor={() => deleteOneAuthor(author.id)}
                  key={author.id}
                  name={author.name}
                />
              ))
            ) : (
              <p>Author list is empty.</p>
            )}
          </div>
        </div>
      </form>
    </div>
  );
}

export default CreateCourse;
