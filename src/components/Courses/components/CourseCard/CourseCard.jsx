import Button from '../../../../common/Button/Button';
import './CourseCard.css';
import pipeDuration from '../../../../helpers/pipeDuration';
import { Link } from 'react-router-dom';

function CourseCard(props) {
  const duration = pipeDuration(props.duration);
  const authNames = props.authors.join(', ');
  const shortDescription = props.description.substring(0, 245);

  return (
    <div className='courseCard'>
      <div className='courseCard__1'>
        <h2>{props.title}</h2>
        <p>{shortDescription}</p>
      </div>
      <div className='courseCard__2'>
        <p>
          <span>Authors:</span> {authNames}
        </p>
        <p>
          <span>Duration:</span> {duration}{' '}
        </p>
        <p>
          <span>Created:</span> {props.creationDate}
        </p>
        <Link to={`/courses/${props.id}`}>
          <Button buttonText='Show course' />
        </Link>
      </div>
    </div>
  );
}

export default CourseCard;
