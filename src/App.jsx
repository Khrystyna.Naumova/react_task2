import Header from './components/Header/Header';
import Courses from './components/Courses/Courses';
import { filteredCoursesData } from './helpers/authorConstructor';
import React, { useState } from 'react';
import CreateCourse from './components/CreateCourse/CreateCourse';
import Login from './components/Login/Login';
import Registration from './components/Registartion/Registration';
import { Routes, Route, Navigate, useNavigate } from 'react-router-dom';
import CourseInfo from './components/CourseInfo/CourseInfo';
import { ProtectedRoute } from './components/Login/protectedRoute';
import { v4 as uuidv4 } from 'uuid';

function App() {
  const [courses, setCourses] = useState(filteredCoursesData);
  const [token, setToken] = useState(null);

  const navigate = useNavigate();

  const loginHandler = (userToken) => {
    if (userToken === 'Invalid data.' || userToken === undefined) {
      alert('You are not registered yet');
    } else {
      localStorage.setItem('token', JSON.stringify(userToken));
      setToken(userToken);
      navigate('/courses', { replace: true });
    }
  };

  const logoutHandler = () => {
    fetch('http://localhost:4000/logout', {
      //???
      method: 'DELETE',
      headers: {
        Authorization: token,
      },
    }).then((res) => console.log(res));

    localStorage.removeItem('token');
    setToken(null);
  };

  const onSaveCourseData = (inputCourseData) => {
    console.log(inputCourseData);
    const newCourse = {
      ...inputCourseData,
      id: uuidv4(),
    };
    setCourses((prevCourses) => {
      return [...prevCourses, newCourse];
    });
  };

  return (
    <div>
      <Header token={localStorage.getItem('token')} onLogout={logoutHandler} />
      <Routes>
        <Route
          path='/'
          element={
            localStorage.getItem('token') ? (
              <Navigate to='/courses' />
            ) : (
              <Navigate to='/login' />
            )
          }
        />
        <Route path='/login' element={<Login onLogin={loginHandler} />} />
        <Route path='/registration' element={<Registration />} />
        <Route
          path='/courses'
          element={
            <ProtectedRoute>
              <Courses courses={courses} />
            </ProtectedRoute>
          }
        />
        <Route
          path='/courses/add'
          element={
            <ProtectedRoute>
              <CreateCourse onSaveCourse={onSaveCourseData} />
            </ProtectedRoute>
          }
        />
        <Route
          path='/courses/:courseId'
          element={<CourseInfo courses={courses} />}
        />
        <Route
          path='*'
          element={<p style={{ fontSize: '1.3rem' }}>Page is not found.</p>}
        />
      </Routes>
    </div>
  );
}

export default App;
